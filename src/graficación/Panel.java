package graficación;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

class Panel extends JPanel {
    Rastreo raster;
    Dimension prefSize;
    public Panel() {
        setBorder(BorderFactory.createLineBorder(Color.black));
    }       

    public Panel(Rastreo _raster) {
        prefSize = new Dimension(_raster.width,_raster.height);
        this.setSize(prefSize);
        this.raster = _raster;
        setBorder(BorderFactory.createLineBorder(Color.black));
    }

    public Dimension getPreferredSize() {
        return prefSize;
    }
    
    public void setRaster(Rastreo _raster){
        this.raster = _raster;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);       
        Image output =  raster.toImage(this);
        g.drawImage(output, 0, 0, this);
    }  
}